function calculerSommeAchats(prixAchats) {
    return prixAchats.reduce(function (total, prix) { return total + prix; }, 0);
}
function rendreMonnaie(sommeDue, sommePayee) {
    var monnaieARendre = sommePayee - sommeDue;
    if (monnaieARendre < 0) {
        return;
    }
    var billets10 = Math.floor(monnaieARendre / 10);
    monnaieARendre %= 10;
    var billets5 = Math.floor(monnaieARendre / 5);
    monnaieARendre %= 5;
    var pieces1 = monnaieARendre;
    for (var i = 0; i < billets10; i++) {
        console.log("10 Euros");
    }
    for (var i = 0; i < billets5; i++) {
        console.log("5 Euros");
    }
    for (var i = 0; i < pieces1; i++) {
        console.log("1 Euros");
    }
}
var prixAchats = [12.99, 8.50, 4.75];
var sommeDue = calculerSommeAchats(prixAchats);
var sommePayee = 50;
rendreMonnaie(sommeDue, sommePayee);

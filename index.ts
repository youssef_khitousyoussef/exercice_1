
function calculerSommeAchats(prixAchats: number[]): number {
    return prixAchats.reduce((total, prix) => total + prix, 0);
}

function rendreMonnaie(sommeDue: number, sommePayee: number): void {
    let monnaieARendre = sommePayee - sommeDue;
    
    if (monnaieARendre < 0) {
        return;
    }


    const billets10 = Math.floor(monnaieARendre / 10);
    monnaieARendre %= 10;
    const billets5 = Math.floor(monnaieARendre / 5);
    monnaieARendre %= 5;
    const pieces1 = monnaieARendre;

    for (let i = 0; i < billets10; i++) {
        console.log("10 Euros");
    }

    for (let i = 0; i < billets5; i++) {
        console.log("5 Euros");
    }

    for (let i = 0; i < pieces1; i++) {
        console.log("1 Euros");
    }
}

const prixAchats = [12.99, 8.50, 4.75];
const sommeDue = calculerSommeAchats(prixAchats);
const sommePayee = 50;

rendreMonnaie(sommeDue, sommePayee);

